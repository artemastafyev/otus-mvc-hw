﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.MVCWebHost.Controllers
{
    public class PartnerController : Controller
    {
        private readonly IPartnerService _partnerService;
        private readonly ILogger<PartnerController> _logger;

        public PartnerController(IPartnerService partnerService,
                                     ILogger<PartnerController> logger)
        {
            _logger = logger;
            _partnerService = partnerService;
        }

        // GET: PreferenceController
        public async Task<ActionResult> Partners()
        {
            var proferences = await _partnerService.GetListAsync();
            return View("Partners", proferences);
        }

        public ActionResult PartialPreferenceList()
        {
            return PartialView("PartialPreferenceList", _partnerService.GetListAsync());
        }

        // GET: PreferenceController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PreferenceController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm] PartnerDTO dto)
        {
            try
            {
                await _partnerService.CreateAsync(dto);
                return RedirectToAction("Partners");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return View(dto);
            }
        }

        // GET: PreferenceController/Edit/5
        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await _partnerService.GetAsync(id);
            return View(model);
        }

        // POST: PreferenceController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([FromForm] PartnerDTO dto)
        {
            try
            {
                await _partnerService.UpdateAsync(dto);
                return RedirectToAction("Partners");
            }
            catch
            {
                return View(dto);
            }
        }

        // GET: PreferenceController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PreferenceController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete([FromForm] PartnerDTO dto)
        {
            try
            {
                await _partnerService.DeleteAsync(dto);
                return RedirectToAction("Partners");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return View(dto);
            }
        }
    }
}
