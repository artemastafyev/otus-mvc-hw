﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.MVCWebHost.Controllers
{
    public class PromoCodeController : Controller
    {
        private readonly IPromoCodeService _promoCodeService;
        private readonly IPreferenceService _preferenceCodeService;
        private readonly IPartnerService _partnerService;
        private readonly ILogger<PromoCodeController> _logger;

        public PromoCodeController(IPromoCodeService promoCodeService,
                                   IPartnerService partnerService,
                                   IPreferenceService preferenceCodeService,
                                   ILogger<PromoCodeController> logger)
        {
            _logger = logger;
            _promoCodeService = promoCodeService;
            _preferenceCodeService = preferenceCodeService;
            _partnerService = partnerService;
        }

        // GET: PreferenceController
        public async Task<ActionResult> PromoCodes()
        {
            var promoCodes = await _promoCodeService.GetListAsync();
            return View("PromoCodes", promoCodes);
        }

        public async Task<ActionResult> Create()
        {
            var partners = await _partnerService.GetListAsync();
            ViewBag.Partners = new SelectList(partners,
                    "Id", "Name");

            var preferences = await _preferenceCodeService.GetListAsync();
            ViewBag.Preferences = new SelectList(preferences,
                    "Id", "Name");

            return View();
        }

        // POST: PreferenceController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm] PromoCodeDTO dto)
        {
            try
            {
                await _promoCodeService.CreateAsync(dto);
                return RedirectToAction("PromoCodes");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return View(dto);
            }
        }

        // GET: PreferenceController/Edit/5
        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await _promoCodeService.GetAsync(id);

            var partners = await _partnerService.GetListAsync();
            ViewBag.Partners = new SelectList(partners,
                    "Id", "Name", model.PartnerId);

            var preferences = await _preferenceCodeService.GetListAsync();
            ViewBag.Preferences = new SelectList(preferences,
                    "Id", "Name", model.PreferenceId);

            return View(model);
        }

        // POST: PreferenceController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([FromForm] PromoCodeDTO dto)
        {
            try
            {
                await _promoCodeService.UpdateAsync(dto);
                return RedirectToAction("PromoCodes");
            }
            catch
            {
                return await Edit(dto.Id);
            }
        }

        // GET: PreferenceController/Delete/5
        public async Task<ActionResult> Delete(Guid id)
        {
            var model = await _promoCodeService.GetAsync(id);
            return View(model);
        }

        // POST: PreferenceController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete([FromForm] PromoCodeDTO dto)
        {
            try
            {
                await _promoCodeService.DeleteAsync(dto);
                return RedirectToAction("PromoCodes");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return View(dto);
            }
        }

    }
}
