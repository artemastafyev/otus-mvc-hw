﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs.UserDTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.MVCWebHost.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserService partnerService, ILogger<UserController> logger)
        {
            _userService = partnerService;
            _logger = logger;
        }

        public async Task<ActionResult> Users()
        {
            var proferences = await _userService.GetListAsync();
            return View("Users", proferences);
        }

        // GET: UserController/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            var user = await _userService.GetAsync(id);
            return View(user);
        }

        // GET: UserController/Create
        public async Task<ActionResult> Create()
        {
            return View();
        }

        // POST: UserController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserDTO model)
        {
            try
            {
                await _userService.CreateAsync(model);

                return RedirectToAction("Users");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: UserController/Edit/5
        public async Task<ActionResult> Edit(Guid id)
        {
            var user = await _userService.GetAsync(id);
            return View(user);
        }

        // POST: UserController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UserDTO model)
        {
            try
            {
                await _userService.UpdateAsync(model);

                return RedirectToAction("Users");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: UserController/Delete/5
        public async Task<ActionResult> Delete(Guid id)
        {
            var user = await _userService.GetAsync(id);
            return View(user);
        }

        // POST: UserController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(UserDTO model)
        {
            try
            {
                await _userService.DeleteAsync(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(model);
            }
        }
    }
}
