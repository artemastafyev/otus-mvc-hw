using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Dictionary.DataAccess;
using Otus.Teaching.Pcf.Dictionary.DataAccess.Data;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using Otus.Teaching.Pcf.Dictionary.Services.PreferenceServices;
using Otus.Teaching.Pcf.Dictionary.Services.Services.PartnerServices;
using Otus.Teaching.Pcf.Dictionary.Services.Services.PromoCodeServices;
using Otus.Teaching.Pcf.Dictionary.Services.Services.UserServices;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.MVCWebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var configuration = Environment.GetEnvironmentVariable("Redis");
            Console.WriteLine(configuration);

            services.AddScoped(typeof(ICacheService<>), typeof(CacheService<>));
            services.AddScoped<IDbInitializer, RedisDbinitializer>();
            services.AddScoped<IPreferenceService, PreferenceService>();
            services.AddScoped<IPromoCodeService, PromoCodeService>();
            services.AddScoped<IPartnerService, PartnerService>();
            services.AddScoped<IUserService, UserService>();

#if DEBUG
            configuration = Configuration["Redis:Configuration"];
#endif

            if (configuration == null)
            {
                configuration = "localhost:6379";
            }

            Console.WriteLine(configuration);

            ConfigurationOptions optionCash = new ConfigurationOptions
            {
                AbortOnConnectFail = false,
                EndPoints = { configuration }
            };

            services.AddStackExchangeRedisCache(option =>
            {
                option.Configuration = configuration;
                option.ConfigurationOptions = optionCash;
            });

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
