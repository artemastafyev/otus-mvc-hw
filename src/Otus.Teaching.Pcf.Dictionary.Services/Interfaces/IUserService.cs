﻿using Otus.Teaching.Pcf.Dictionary.Services.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Interfaces
{
    public interface IUserService : ICRUD<GetUserDTO, UserDTO>
    {
    }
}
