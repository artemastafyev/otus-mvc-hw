﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Interfaces
{
    public interface ICRUD<DTO>
    {
        Task<IEnumerable<DTO>> GetListAsync();

        Task<DTO> GetAsync(Guid id);

        Task CreateAsync(DTO model);

        Task UpdateAsync(DTO model);

        Task DeleteAsync(DTO model);

    }

    public interface ICRUD<GetDTO, DTO>
    {
        Task<IEnumerable<GetDTO>> GetListAsync();

        Task<DTO> GetAsync(Guid id);

        Task CreateAsync(DTO model);

        Task UpdateAsync(DTO model);

        Task DeleteAsync(DTO model);

    }
}
