﻿using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs.PromoCodeDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Interfaces
{
    public interface IPromoCodeService : ICRUD<GetPromoCodeDTO, PromoCodeDTO>
    {
    }
}
