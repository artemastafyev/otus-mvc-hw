﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using Otus.Teaching.Pcf.Dictionary.DataAccess;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs.UserDTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using Otus.Teaching.Pcf.Dictionary.Services.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Services.UserServices
{
    public class UserService : IUserService
    {
        private readonly ICacheService<User> _partners;

        public UserService(ICacheService<User> partners)
        {
            _partners = partners;
        }

        public async Task<IEnumerable<GetUserDTO>> GetListAsync()
        {
            var partners = await this._partners.GetListAsync();

            return partners.Select(x => new GetUserDTO(x));
        }

        public async Task<UserDTO> GetAsync(Guid id)
        {
            User model = await Validation(id);

            return new UserDTO(model);
        }

        public async Task CreateAsync(UserDTO model)
        {
            await _partners.AddAsync(model);
        }

        public async Task UpdateAsync(UserDTO model)
        {
            await Validation(model.Id);

            await _partners.UpdateAsync(model);
        }

        public async Task DeleteAsync(UserDTO model)
        {
            await Validation(model.Id);

            await _partners.RemoveAsync(model);
        }

        private async Task<User> Validation(Guid id)
        {
            var model = await _partners.GetByIdAsync(id);

            if (model == null)
            {
                throw new EntityIsNotFoundException("Запись партнера отсутствует!");
            }

            return model;
        }
    }
}
