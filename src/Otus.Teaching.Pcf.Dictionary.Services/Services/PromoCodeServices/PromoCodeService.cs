﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using Otus.Teaching.Pcf.Dictionary.DataAccess;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs.PromoCodeDTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using Otus.Teaching.Pcf.Dictionary.Services.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Services.PromoCodeServices
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly ICacheService<PromoCode> _promocodes;
        private readonly ICacheService<Preference> _preferences;
        private readonly ICacheService<Partner> _partners;

        public PromoCodeService(ICacheService<PromoCode> promocodes,
                                ICacheService<Preference> preferences,
                                ICacheService<Partner> partners)
        {
            _promocodes = promocodes;
            _preferences = preferences;
            _partners = partners;
        }

        public async Task<IEnumerable<GetPromoCodeDTO>> GetListAsync()
        {
            var promoCodes = await _promocodes.GetListAsync();
            var preferences = await _preferences.GetListAsync();
            var partners = await _partners.GetListAsync();


            return promoCodes.Select(x => new GetPromoCodeDTO(x)
            {
                PartnerName = partners.FirstOrDefault(p => p.Id == x.PartnerId)?.Name,
                PreferenceName = preferences.FirstOrDefault(p => p.Id == x.PreferenceId)?.Name
            });
        }
        public async Task<PromoCodeDTO> GetAsync(Guid id)
        {
            PromoCode model = await Validation(id);

            return new PromoCodeDTO(model);
        }

        public async Task CreateAsync(PromoCodeDTO model)
        {
            await _promocodes.AddAsync(model);
        }

        public async Task UpdateAsync(PromoCodeDTO model)
        {
            await Validation(model.Id);

            await _promocodes.UpdateAsync(model);
        }

        public async Task DeleteAsync(PromoCodeDTO model)
        {
            await Validation(model.Id);

            await _promocodes.RemoveAsync(model);
        }

        private async Task<PromoCode> Validation(Guid id)
        {
            var model = await _promocodes.GetByIdAsync(id);

            if (model == null)
            {
                throw new EntityIsNotFoundException("Запись промокодов отсутствует!");
            }

            return model;
        }
    }
}
