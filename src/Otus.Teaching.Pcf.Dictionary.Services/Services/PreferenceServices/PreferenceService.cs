﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using Otus.Teaching.Pcf.Dictionary.DataAccess;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using Otus.Teaching.Pcf.Dictionary.Services.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.PreferenceServices
{
    public class PreferenceService : IPreferenceService
    {
        private readonly ICacheService<Preference> _preferences;

        public PreferenceService(ICacheService<Preference> preferences)
        {
            _preferences = preferences;
        }

        public async Task<IEnumerable<PreferenceDTO>> GetListAsync()
        {
            var preferences = await _preferences.GetListAsync();

            return preferences.Select(x => new PreferenceDTO(x));
        }

        public async Task<PreferenceDTO> GetAsync(Guid id)
        {
            Preference model = await Validation(id);

            return new PreferenceDTO(model);
        }

        public async Task CreateAsync(PreferenceDTO model)
        {
            await _preferences.AddAsync(model);
        }

        public async Task UpdateAsync(PreferenceDTO model)
        {
            await Validation(model.Id);

            await _preferences.UpdateAsync(model);
        }

        public async Task DeleteAsync(PreferenceDTO model)
        {
            await Validation(model.Id);

            await _preferences.RemoveAsync(model);
        }

        private async Task<Preference> Validation(Guid id)
        {
            var model = await _preferences.GetByIdAsync(id);

            if (model == null)
            {
                throw new EntityIsNotFoundException("Запись предпочтений отсутствует!");
            }

            return model;
        }
    }
}
