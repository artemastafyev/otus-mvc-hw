﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using Otus.Teaching.Pcf.Dictionary.DataAccess;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using Otus.Teaching.Pcf.Dictionary.Services.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Services.PartnerServices
{
    public class PartnerService : IPartnerService
    {
        private readonly ICacheService<Partner> _partners;

        public PartnerService(ICacheService<Partner> partners)
        {
            _partners = partners;
        }

        public async Task<IEnumerable<PartnerDTO>> GetListAsync()
        {
            var partners = await this._partners.GetListAsync();

            return partners.Select(x => new PartnerDTO(x));
        }

        public async Task<PartnerDTO> GetAsync(Guid id)
        {
            Partner model = await Validation(id);

            return new PartnerDTO(model);
        }

        public async Task CreateAsync(PartnerDTO model)
        {
            await _partners.AddAsync(model);
        }

        public async Task UpdateAsync(PartnerDTO model)
        {
            await Validation(model.Id);

            await _partners.UpdateAsync(model);
        }

        public async Task DeleteAsync(PartnerDTO model)
        {
            await Validation(model.Id);

            await _partners.RemoveAsync(model);
        }

        private async Task<Partner> Validation(Guid id)
        {
            var model = await _partners.GetByIdAsync(id);

            if (model == null)
            {
                throw new EntityIsNotFoundException("Запись партнера отсутствует!");
            }

            return model;
        }
    }
}
