﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.DTOs.PromoCodeDTOs
{
    public class GetPromoCodeDTO : BaseDTO
    {
        public GetPromoCodeDTO(PromoCode model)
        {
            Id = model.Id;
            Code = model.Code;
            ServiceInfo = model.ServiceInfo;
            BeginDate = model.BeginDate;
            EndDate = model.EndDate;
            PartnerName = model.Partner?.Name;
            PreferenceName = model.Preference?.Name;
        }

        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string PartnerName { get; set; }

        public string PreferenceName { get; set; }
    }
}
