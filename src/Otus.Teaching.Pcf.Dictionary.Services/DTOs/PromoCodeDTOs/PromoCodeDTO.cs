﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.DTOs
{
    public class PromoCodeDTO : BaseDTO
    {
        public PromoCodeDTO()
        {

        }

        public PromoCodeDTO(PromoCode model)
        {
            Id = model.Id;
            Code = model.Code;
            ServiceInfo = model.ServiceInfo;
            BeginDate = model.BeginDate;
            EndDate = model.EndDate;
            PartnerId = model.PartnerId;
            PreferenceId = model.PreferenceId;
        }

        [Display(Name = "Код"),Required]
        public string Code { get; set; }

        [Display(Name = "Дополнительная информация"), Required]
        public string ServiceInfo { get; set; }

        [Display(Name = "Начала действие"), Required]
        public DateTime BeginDate { get; set; }

        [Display(Name = "Окончание действие"), Required]
        public DateTime EndDate { get; set; }

        [Display(Name = "Партнер"), Required]
        public Guid PartnerId { get; set; }

        [Display(Name = "Предпочтение"), Required]
        public Guid PreferenceId { get; set; }

        public static implicit operator PromoCode(PromoCodeDTO dto)
        {
            return new PromoCode()
            {
                Id = dto.Id,
                Code = dto.Code,
                ServiceInfo = dto.ServiceInfo,
                BeginDate = dto.BeginDate,
                EndDate = dto.EndDate,
                PartnerId = dto.PartnerId,
                PreferenceId = dto.PreferenceId,
            };
        }
    }
}
