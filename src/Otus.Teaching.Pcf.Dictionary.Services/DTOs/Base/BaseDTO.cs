﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.DTOs
{
    public class BaseDTO
    {
        [Display(Name = "Идентификатор")]
        public Guid Id { get; set; }
    }
}
