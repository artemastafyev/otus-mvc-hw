﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.DTOs.UserDTOs
{
    public class UserDTO : BaseDTO
    {
        public UserDTO()
        {

        }

        public UserDTO(User user)
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
        }

        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Поле email обязательное")]
        [EmailAddress(ErrorMessage = "Не корректный Email")]
        public string Email { get; set; }

        public static implicit operator User(UserDTO dto)
        {
            return new User()
            {
                Id = dto.Id,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email
            };
        }
    }
}
