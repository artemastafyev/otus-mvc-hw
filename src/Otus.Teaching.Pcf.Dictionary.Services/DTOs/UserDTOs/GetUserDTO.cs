﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.DTOs.UserDTOs
{
    public class GetUserDTO : BaseDTO
    {
        public GetUserDTO(User user)
        {
            FullName = user.FullName;
            Email = user.Email;
            Id = user.Id;
        }

        [Display(Name = "ФИО")]
        public string FullName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
