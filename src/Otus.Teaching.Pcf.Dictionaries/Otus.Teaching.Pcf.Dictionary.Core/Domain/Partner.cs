﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Core.Domain
{
    public class Partner
        : BaseEntity
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
