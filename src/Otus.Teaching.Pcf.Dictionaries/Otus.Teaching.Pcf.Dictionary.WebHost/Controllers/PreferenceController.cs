﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using Otus.Teaching.Pcf.Dictionary.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Otus.Teaching.Pcf.Dictionary.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferenceController : ControllerBase
    {
        private readonly ICacheService<Preference> _preferences;

        public PreferenceController(ICacheService<Preference> preferences)
        {
            _preferences = preferences;
        }

        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Preference>> Get()
        {
            return await _preferences.GetListAsync();
        }

        /// <summary>
        /// Получить конкретное предпочтение
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<Preference> Get(Guid id)
        {
            return await _preferences.GetByIdAsync(id);
        }

        /// <summary>
        /// Редактировать предпочтение
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task Post([FromBody] Preference value)
        {
            await _preferences.AddAsync(value);
        }

        /// <summary>
        /// Добавить новое предпочтение
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task Put([FromBody] Preference value)
        {
            await _preferences.UpdateAsync(value);
        }

        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            await _preferences.RemoveAsync(await _preferences.GetByIdAsync(id));
        }
    }
}
